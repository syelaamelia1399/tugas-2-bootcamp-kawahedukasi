import java.util.Scanner;

public class HasilPangkat {
    public static void main(String[] args) {
        Scanner angka1 = new Scanner(System.in);
        System.out.print("Masukkan angka basis : ");
        int basis = angka1.nextInt();

        Scanner angka2 = new Scanner(System.in);
        System.out.print("Masukkan angka pangkat : ");
        int pangkat = angka2.nextInt();

        System.out.println("Hasil " + basis + " pangkat " + pangkat + " adalah " + perpangkatan(basis, pangkat));
        angka1.close();
        angka2.close();
    }

    public static int perpangkatan(int basis, int pangkat) {
        int i;
        int hasil = 1;
        for (i = 0; i < pangkat; i++) {
            hasil *= basis;
        }
        return hasil;
    }
}
