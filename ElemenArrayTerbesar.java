import java.util.Scanner;

public class ElemenArrayTerbesar {
    public static void main(String[] args) {
        Scanner angka = new Scanner(System.in);

        int[] arr = new int[100];
        int arr_count, i, max_num;

        System.out.print("Input jumlah elemen array : ");
        arr_count = angka.nextInt();

        System.out.println("Input " + arr_count + " angka (dipisah dengan enter) :");

        for (i = 0; i < arr_count; i++) {
            arr[i] = angka.nextInt();
        }

        max_num = arr[0];

        for (i = 0; i < arr_count; i++) {
            if (arr[i] > max_num) {
                max_num = arr[i];
            }
        }
        angka.close();

        System.out.println("Angka terbesar adalah " + max_num);
        
    }
}