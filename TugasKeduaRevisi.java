public class TugasKeduaRevisi {
    public static void main(String[] args) {
        System.out.println(pangkat(3, 2));
    }

    public static int nilaiMutlak(int angka) {

        if (angka < 0) {
            return angka * -1;
        }

        return angka;
    }

    public static int angkaTerbesar(int[] arrayA) {

        int angkaTerbesar = arrayA[0];

        for (int angka : arrayA) {
            if (angkaTerbesar < angka) {
                angkaTerbesar = angka;
            }
        }

        return angkaTerbesar;
    }

    public static int pangkat(int basis, int eksponen) {

        int hasil = 1;

        for (int i = 0; i < eksponen; i++) {
            hasil *= basis;
        }

        return hasil;
    }
}
