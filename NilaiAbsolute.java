import java.util.Scanner;

public class NilaiAbsolute {
    public static void main(String[] args) {
        Scanner nilaiMutlak = new Scanner(System.in);
        System.out.print("Masukkan angka yang akan dicari nilai mutlaknya : ");
        int angka = nilaiMutlak.nextInt();
        
        System.out.println("Nilai mutlak dari " + angka + " adalah " + mutlak(angka));
        nilaiMutlak.close();
    }

    public static int mutlak(int angka) {
        if (angka < 0) {
            return -angka;
        } else {
            return angka;
        }
    }
}